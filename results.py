class Result(object):
    profile = {
        "uniqueId": str,
        "nickname": str,
        "avatarLarger": str,
        "avatarMedium": str,
        "avatarThumb": str,
        "signature": str,
        "createTime": str,
        "verified": str,
        "secUid": str,
        "ftc": str,
        "relation": str,
        "openFavorite": str,
        "bioLink": {
          "link": {

          }
        },
        "commentSetting": str,
        "duetSetting": str,
        "stitchSetting": str,
        "privateAccount": str,
        "secret": str,
        "stats": str

    }
    stats = {
        "followerCount": str,
        "followingCount": str,
        "heart": str,
        "heartCount": str,
        "videoCount": str,
        "diggCount": str
    }
    bioLink = {
        "link": str
    }