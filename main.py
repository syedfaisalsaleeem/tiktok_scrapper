

from configurations import browser_cookies_s_v_web_id
from TikTokApi import TikTokApi
from results import Result
from Resultclass import Resultclass

print(browser_cookies_s_v_web_id)

class User:
    def __init__(self, cookies):
        self.api = TikTokApi.get_instance(custom_verifyFp=cookies)
        self.id = ""
        self.secUid = ""
        # self.temp_dict = {}
    def getUserApi(self,username):
        tick = self.api.getUser(username=username)
        return tick
    def getuser(self, username):
        try:
            tick = self.getUserApi(username=username)

            for key, values in Result.bioLink.items():
                if(tick["userInfo"]["user"]["bioLink"].get(key) != None):
                    Result.bioLink[key] = tick["userInfo"]["user"]["bioLink"][key]
                else:
                    Result.bioLink[key] = ""

            for key, values in Result.stats.items():
                if(tick["userInfo"]["stats"].get(key) != None):
                    Result.stats[key] = tick["userInfo"]["stats"][key]
                else:
                    Result.stats[key] = ""

            for key, values in Result.profile.items():
                if(tick["userInfo"]["user"].get(key) != None):
                    Result.profile[key] = tick["userInfo"]["user"][key]
                else:
                    Result.profile[key] = ""
            Result.profile["bioLink"] = Result.bioLink
            Result.profile["stats"] = Result.stats

            return Result.profile

        except:
            return Result.profile

    def getUserVideos(self, username, limit=None):
        videosList = Resultclass(videolist=[])
        try:
            userdescription = self.getUserApi(username=username)
            self.id = userdescription['userInfo']['user']['id']
            self.secUid = userdescription['userInfo']['user']['secUid']
            print(limit)
            if limit is None:
                videoslen = userdescription['userInfo']['stats']['videoCount']

            else:
                videoslen = limit
            print(videoslen)
            userPosts = self.api.userPosts(
                self.id,
                self.secUid,
                videoslen
            )
            videosList.videos = userPosts
            return videosList.videos
        except Exception as e:
            print(e)
            return videosList.videos



    def getTrends(self,limit=None):
        try:
            if limit is None:
                tick = self.api.trending(count=10, language='en', proxy=None, custom_verifyFp="")
                print(len(tick))
                return tick
            else:
                tick = self.api.trending(count=limit, language='en', proxy=None, custom_verifyFp="")
                print(len(tick))
                return tick
        except Exception as e:
            print(e)
            return []


    def getSearchResult(self, search_query, count):
        self.temp_list = []
        self.dict_test = {}
        self.t = ""
        try:
            tick = self.api.search_for_users(search_term=search_query, count=count)
            # print(tick)
            for value, users in enumerate(tick):
                for key, values in Result.profile.items():
                    # print(key, values)
                    if(users['user'].get(key) != None):
                        Result.profile[key] = users['user'][key]
                    else:
                        Result.profile[key] = ""

                # Result.profile["stats"] = users['user']['uniqueId']
                # print(Result.profile["stats"])
                for key, values in Result.stats.items():
                    # print(key)
                    if (users["stats"].get(key) != None):
                        Result.stats[key] = users["stats"][key]
                    else:
                        Result.stats[key] = ""
                Result.profile['stats'] = dict(Result.stats)
                self.temp_list.append(dict(Result.profile))

            return self.temp_list

        except Exception as e:
            print(e)
            return self.temp_list

    def searchAll(self):
        discover = self.api.discoverHashtags()
        print(discover)
        # discover_type(self, search_term, prefix, count=28, **kwargs)

    def getsuggestedUsers(self):
        n_suggestions = 100
        username = 'kourtneykardashian'
        user_id = self.api.getUser(username)['userInfo']['user']['id']
        suggested = self.api.getSuggestedUsersbyID(count=n_suggestions, user_id=user_id)
        print(suggested)

    def allLikesUser(self):
        try:
            # likes = 10
            likes = self.api.userLikedbyUsername(username="rahat__khan7")
            # print(likes)
            return likes
        except Exception as e:
            print(e)
            return "Users are private"

    def searchByHashtags(self): ## return all the similar hashtags
        hashtags = self.api.search_for_hashtags(search_term="funny")
        print(hashtags)
        '''
            search by hashtag is not returned
            hashtag = self.api.byHashtag("funny",count=10)
            print(hashtag)
        '''

        # searchHashtag = self.api.byHashtag("funny",language='en',count=10,custom_verifyFp="verify_klt2r0le_31bOJx0S_aZy8_4zpE_AlEk_IDKb8Vu4zXfi",proxy=None)
        # # for x in searchHashtag:
        #     # print(x)
        # print(searchHashtag)



if __name__ == '__main__':
    x = User(cookies=browser_cookies_s_v_web_id)

    #### Get Users Data ########
    # y = x.getuser(username="washingtonpost")
    # print(y)

    ####### Get all the users named with given parameter ############
    # y = x.getSearchResult(search_query="Faisal", count=10)
    # print(y)

    ###### Get all User Videos with the given username ##############
    # y = x.getUserVideos(username="washingtonpost", limit=3)
    # print(y)

    ####### Get all the trending videos ##############
    # y = x.getTrends(limit=4)
    # print(y)

    # y = x.searchAll()
    # print(y)

    ######## Get all the similar hashtags ###########
    # y = x.searchByHashtags()
    # print(y)

    ####### Get all the similar users ##########
    # y = x.getsuggestedUsers()

    ##### Get all the users or videos are not available in Api ######

    ###### Get all the likes done by the user ########
    # y = x.allLikesUser()
    # print(y)




