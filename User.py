import json
import dictfier


class Course(object):
    def __init__(self, name):
        self.name = name


class User(object):
    def __init__(self, name, username, course):
        self.name = name
        self.username = username
        self.course = course

    def getJson(self):
        temp_dict = {
            "message": "success"
        }
        text = json.dumps(temp_dict,sort_keys=True,indent=4)
        print(text)


c = Course("")
x = User("f", "2", [c])
query = [
    "name",
    "username",
    {
        "course": [
            [
                "name"
            ]
        ]
    }
]
y = dictfier.dictfy(x, query)
text = json.dumps(y, sort_keys=True, indent=4)
print(y)
print(text)
