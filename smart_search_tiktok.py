from TikTokApi import TikTokApi

class SmartSearchTikTok():
    def __init__(self):
        self.api = TikTokApi.get_instance()
        self.userId = ""
        self.userName = ""
        self.profileImageUrl = ""
        self.profileUrl = ""
        self.snapchatdict = {
            "userId": self.userId,
            "userName": self.userName,
            "profileImageUrl": self.profileImageUrl,
            "profileUrl": self.profileUrl
        }

    def getUserApi(self, username):
        tick = self.api.getUserObject(username=username)
        try:
            self.userId = tick['uniqueId']
            self.userName = tick['nickname']
            self.profileImageUrl = tick['avatarLarger']
            self.profileUrl = tick['bioLink']['link']
            self.snapchatdict["userId"] = self.userId
            self.snapchatdict["userName"] = self.userName
            self.snapchatdict["profileImageUrl"] = self.profileImageUrl
            self.snapchatdict["profileUrl"] = self.profileUrl
            return self.snapchatdict
        except Exception as e:
            print("error", e)
            return self.snapchatdict


y = SmartSearchTikTok()
x = y.getUserApi(username="johnyblueyes")
print(x)


